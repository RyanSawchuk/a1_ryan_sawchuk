import logo from './logo.svg';
import './App.css';

function insert_row() {
  var table = document.getElementById('inner-body');
  table.append("<tr><td>Chicken Breast</td><td>330g</td><td>$8.99</td><td>165</td><td>35g</td><td>3.6g</td></tr>");
}


function App() {
  return (
    <div className="App">
      <div class="navbar">
        <a class="active" href="">Home</a>
        <a href="">User Profile</a>
        <a href="">Logout</a>
      </div>
      <div class="top-container">
        <div>
          <h4>Chicken Alfredo Pasta</h4>
        </div>
        <div>
          <button id="button1">Upload Recipe</button>
        </div>
        <div>
          <button id="button2">Download Recipe</button>
        </div>
        <div>
          <button id="button3">Add Items to Cart</button>
        </div>
      </div>
      <br></br>
      <div class="table-container">
        <table id="info-table">
          <caption>Recipe Ingredient Information</caption>
          <tbody id="inner-body">
          <tr class="columheaders">
            <th>Ingredient</th>
            <th>Amount</th>
            <th>Cost</th>
            <th>Calories</th>
            <th>Protien</th>
            <th>Carbs</th>
            <th>Fat</th>
          </tr>
          <tr class="row">
            <th>linguine noodles</th>
            <th>12g</th>
            <th>$2.00</th>
            <th></th>
            <th>7g</th>
            <th>42g</th>
            <th>1g</th>
          </tr>
          <tr class="row">
            <th>Unsalted Butter</th>
            <th>1 stick</th>
            <th>$5.00</th>
            <th>102</th>
            <th>0g</th>
            <th>0.1g</th>
            <th>81g</th>
          </tr>
          <tr class="row">
            <th>Salt and Pepper</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
          <tr class="row">
            <th>Heavy Cream</th>
            <th>2 cups</th>
            <th>$5.00</th>
            <th>400</th>
            <th>3g</th>
            <th>3g</th>
            <th>43g</th>
          </tr>
          <tr class="row">
            <th>Parmigiano-Reggiano</th>
            <th>1 1/2 cups</th>
            <th>$20.00</th>
            <th>431</th>
            <th>38g</th>
            <th>4.1g</th>
            <th>29g</th>
          </tr>
          </tbody>
        </table>
      </div>
      <br></br>
      <div class="add-ingredient-container">
        <input type="text" placeholder="Ingredient Search ..."></input>
        <h3>Raw Skinnless Chicken Breast</h3>
        <img src="raw_chicken.jpg"></img>
        <p>Quantity: 2</p>
        <p>Cost: $8.99</p>
        <p>Calories: 165</p>
        <p>Protien: 35g</p>
        <p>Fat: 3.6g</p>
        <button class="add-ingredient" onClick={insert_row}>Add Ingredient</button>
      </div>
    </div>
  );
}


export default App;
